This is my daily color scheme.

There are two variants; One is the daily driver and one helps me if I get sick of colors.
Note that the latter is not in daily use and probably suffers from laziness (silentcobalt).

~There is no license attached~ Public Domain.
When I took inspiration of cobalt 2 (microsoft) I did check for licenses - but I can not recall which one they use.

It is very bare-bone and therefore should work with any language.
I am using it for ALGOL-like, Scheme and markup languages.

![preview](https://zamba.hagiga.de/show.svg).

